<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >

	<UiMod name="MoraleSet" version="1.0" date="9/28/2007" >
		<Author name="Reivan" email="" />
		<Description text="Adds a set functionality to morale abilities, similar to tactics." />
		<Dependencies>
			<Dependency name="EASystem_Tooltips" />
		</Dependencies>
		<Files>
			<File name="Source/MoraleSet.xml" />
		</Files>
		<SavedVariables>
			<SavedVariable name="MoraleSetData" />
		</SavedVariables>
		
		<OnInitialize>
			<CreateWindow name="MoraleEditor" show="true" />
		</OnInitialize>
	</UiMod>

</ModuleFile>
